import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class Tests {
    WebDriver driver;
    WebElement errorElement;
    WebElement firstName;
    WebElement lastName;
    WebElement personalCode;
    WebElement male;
    WebElement female;
    WebElement address;
    WebElement phoneNumber;
    WebElement studyProgram;
    WebElement regularStudies;
    WebElement onlineStudies;
    WebElement button;

    @Before
    public void init() {
        driver = new SafariDriver();
        driver.get("https://stanul.com/form.html");
        errorElement = driver.findElement(By.id("error"));
        firstName = driver.findElement(By.id("firstName"));
        lastName = driver.findElement(By.id("lastName"));
        personalCode = driver.findElement(By.id("pinCode"));
        male = driver.findElement(By.id("gender_0"));
        female = driver.findElement(By.id("gender_1"));
        address = driver.findElement(By.id("address"));
        phoneNumber = driver.findElement(By.id("phoneNumber"));
        studyProgram = driver.findElement(By.id("studyProgram"));
        regularStudies = driver.findElement(By.id("formOfStudies_0"));
        onlineStudies = driver.findElement(By.id("formOfStudies_1"));
        button = driver.findElement(By.id("button"));
    }

    //All empty
    @Test
    public void ButtonClick_Empty_LotsOfWarnings() {
        button.click();
        assertThat(errorElement.getText(), containsString("First name is required"));
        assertThat(errorElement.getText(), containsString("Last name is required"));
        assertThat(errorElement.getText(), containsString("Personal code is required"));
        assertThat(errorElement.getText(), containsString("Gender is required"));
        assertThat(errorElement.getText(), containsString("Address is required"));
        assertThat(errorElement.getText(), containsString("Phone number is required"));
        assertThat(errorElement.getText(), containsString("Form of studies if required"));
        assertThat(errorElement.getText(), containsString("Enter international format phone number"));
        assertThat(errorElement.getText(), containsString("Personal code is wrong length"));
        assertThat(errorElement.getText(), containsString("Personal code should contain only numbers"));
        assertThat(errorElement.getText(), containsString("Messed up pin. This date doesn't exist"));
    }

    //Test single selection
    @Test
    public void ButtonClick_OnlyFirstName_SpecificWarning() {
        firstName.sendKeys("Ivan");
        button.click();
        assertThat(errorElement.getText(), not(containsString("First name is required")));
    }

    @Test
    public void ButtonClick_OnlyLastName_SpecificWarning() {
        lastName.sendKeys("Stanul");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Last name is required")));
    }

    @Test
    public void ButtonClick_OnlyGoodCode_SpecificWarning() {
        personalCode.sendKeys("39109020083");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), not(containsString("Personal code is wrong length")));
        assertThat(errorElement.getText(), not(containsString("Personal code should contain only numbers")));
        assertThat(errorElement.getText(), not(containsString("Messed up pin. This date doesn't exist")));
        assertThat(errorElement.getText(), not(containsString("Checksum doesn't match, personal code is wrong.")));
    }

    @Test
    public void ButtonClick_OnlyMale_SpecificWarning() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", male);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Gender is required")));
    }

    @Test
    public void ButtonClick_OnlyFemale_SpecificWarning() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", female);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Gender is required")));
    }

    @Test
    public void ButtonClick_OnlyAddress_SpecificWarning() {
        address.sendKeys("Kaunas, Kauno g. 12");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Address is required")));
    }

    @Test
    public void ButtonClick_OnlyGoodPhone_SpecificWarning() {
        phoneNumber.sendKeys("+37067676793");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Phone number is required")));
    }

    @Test
    public void ButtonClick_OnlyStudyProgram_SpecificWarning() {
        Select program = new Select(studyProgram);
        program.selectByIndex(2);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Study program is required")));
    }

    @Test
    public void ButtonClick_OnlyFormRegular_SpecificWarning() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", regularStudies);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Form of studies if required")));
    }

    @Test
    public void ButtonClick_OnlyFormOnline_SpecificWarning() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", onlineStudies);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Form of studies if required")));
    }


    //Test if exists
    @Test
    public void ButtonClick_TestAddingSameStudentTwice_SpecificWarning() {
        firstName.sendKeys("Ivan");
        lastName.sendKeys("Stanul");
        personalCode.sendKeys("39109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", male);
        address.sendKeys("Kaunas, Kauno g. 12");
        phoneNumber.sendKeys("+37067676793");
        Select program = new Select(studyProgram);
        program.selectByIndex(2);
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", regularStudies);
        button.click();
        firstName.sendKeys("Someone");
        lastName.sendKeys("Else");
        personalCode.sendKeys("39109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", male);
        address.sendKeys("Kaunas, Kauno g. 12");
        phoneNumber.sendKeys("+37067676793");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", regularStudies);
        assertThat(errorElement.getText(), not(containsString("Student with this personal code is already registered")));
    }

    //gender discrimination check
    @Test
    public void ButtonClick_CodeWithMatchingGenderMale_SpecificWarning() {
        personalCode.sendKeys("39109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", male);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), not(containsString("Your gender doesn't match personal code.")));
    }

    @Test
    public void ButtonClick_CodeWithMatchingGenderFemale_SpecificWarning() {
        personalCode.sendKeys("49109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", female);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), not(containsString("Your gender doesn't match personal code.")));
    }

    @Test
    public void ButtonClick_CodeWithNotMatchingGenderMale_SpecificWarning() {
        personalCode.sendKeys("49109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", male);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Your gender doesn't match personal code."));
        assertThat(errorElement.getText(), containsString("No trans are allowed in this school."));
    }

    @Test
    public void ButtonClick_CodeWithNotMatchingGenderFemale_SpecificWarning() {
        personalCode.sendKeys("39109020083");
        ((JavascriptExecutor) driver).executeScript("arguments[0].checked = true;", female);
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Your gender doesn't match personal code."));
        assertThat(errorElement.getText(), containsString("No trans are allowed in this school."));
    }

    //phone test
    @Test
    public void ButtonClick_GoodPhone_SpecificWarning() {
        phoneNumber.sendKeys("+37067676793");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Phone number is required")));
        assertThat(errorElement.getText(), not(containsString("Enter international format phone number")));
    }

    @Test
    public void ButtonClick_BadPhoneWithLetter_SpecificWarning() {
        phoneNumber.sendKeys("+370AS676793");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Phone number is required")));
        assertThat(errorElement.getText(), containsString("Enter international format phone number"));
    }

    @Test
    public void ButtonClick_BadPhoneTooLong_SpecificWarning() {
        phoneNumber.sendKeys("+3706767676767676767676767");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Phone number is required")));
        assertThat(errorElement.getText(), containsString("Enter international format phone number"));
    }

    //personal code test
    @Test
    public void ButtonClick_CodeWithLetters_SpecificWarning() {
        personalCode.sendKeys("3A109020083");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Personal code should contain only numbers"));
    }

    @Test
    public void ButtonClick_CodeTooLong_SpecificWarning() {
        personalCode.sendKeys("391090200831111111");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Personal code is wrong length"));
    }

    @Test
    public void ButtonClick_CodeWithFakeDate_SpecificWarning() {
        personalCode.sendKeys("39102420083");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Messed up pin. This date doesn't exist"));
    }

    @Test
    public void ButtonClick_CodeWithBadChecksum_SpecificWarning() {
        personalCode.sendKeys("39102420080");
        button.click();
        assertThat(errorElement.getText(), not(containsString("Personal code is required")));
        assertThat(errorElement.getText(), containsString("Checksum doesn't match, personal code is wrong."));
    }

    @After
    public void Shutdown() {
        driver.quit();
    }
}
